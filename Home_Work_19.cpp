﻿#include <iostream>

class Animal {
public :
    Animal()
    {
        std::cout << "Animal: \n";
    }
};

class Dog : Animal {
public:
    Dog()
    {
        std::cout << "Dog say: Woof!\n";
    }
};

class Snake : Animal {
public:
    Snake()
    {
        std::cout << "Snake say: Sshhhhhh...\n";
    }
};

class Fox : Animal {
public:
    Fox()
    {
        std::cout << "What Does The Fox Say?\n";
    }
};

int main()
{
    Dog* say = new Dog;
    Snake* say1 = new Snake;
    Fox* say2 = new Fox;
}